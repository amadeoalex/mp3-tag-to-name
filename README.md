# MP3 Tag To Name

Extracts ID3v1 tag data from mp3 files and creates copy of them with name composed of two parts, artist and title. ("ARTIST - TITLE.mp3").
Currently only ID3v1 (excluding TAG+) standard is supported.

(if given file does not contain required data, at the end of execution user is asked if he/she wants to input missing data manually)

## Project status

 - [x] Finished
 - [x] Pending rewrite (at some point in the future)

## Compilation

### Compiler used

	g++ (x86_64-posix-seh-rev0, Built by MinGW-W64 project) 8.1.0

### Compile with

	g++ main.cpp MP3File.cpp StringUtils.cpp -oMP3TagToName.exe -std=c++17 -Wall -Wextra

## Usage

Parameters:

* **[WORKING DIRECTORY]** - directory where mp3 files are located
* **[OUTPUT DIRECTORY]** - directory where mp3 files are located (if not specified defaults to [WORKING DIRECTORY]/renamed/)

Passing no arguments to the program causes it to print out basic instructions.

### Example

Go through all files present in the "music" folder and create copies with modified name in the "music/renamed" directory.

	MP3TagToName.exe music

Go through all files present in the "music" folder and create copies with modified name in the "E:/modified" directory.

	MP3TagToName.exe music "E:/modified"

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.