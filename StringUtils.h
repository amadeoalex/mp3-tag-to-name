#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>

namespace StringUtils {

std::string trim(const std::string& source);
bool startsWith(const std::string& source, const std::string& find);
bool endsWith(const std::string& source, const std::string& find);
}

#endif