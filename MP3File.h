#ifndef MP3File_H
#define MP3File_H

#include <fstream>
#include <string>
#include <utility>
#include <vector>

class MP3File {
    std::string getID3v1Block();
    void parseID3v1(const std::string& ID3v1Block);
    void copyTAG(const std::string& content, std::pair<int, int>& where);

  public:
    std::pair<int, int> titleRange = std::make_pair(3, 30);
    std::pair<int, int> artistRange = std::make_pair(33, 30);
    std::pair<int, int> albumRange = std::make_pair(63, 30);
    std::pair<int, int> yearRange = std::make_pair(93, 4);
    std::pair<int, int> commentRange = std::make_pair(97, 30);
    std::pair<int, int> genreRange = std::make_pair(127, 1);

    std::vector<char> fileData;

    std::string title;
    std::string artist;
    std::string album;
    std::string year;
    std::string comment;
    std::string track;
    std::string genre;

    MP3File() = default;
    MP3File(std::ifstream& fileStream);

    void updateID3v1Block();
};

#endif