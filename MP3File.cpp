#include "MP3File.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "StringUtils.h"

std::string MP3File::getID3v1Block()
{
    if (fileData.size() == 0) {
        puts("No fileData loaded!");
        return "";
    }

    auto start = fileData.end() - 128;
    auto end = fileData.end();

    std::string ID3v1 = std::string(start, end);

    return ID3v1;
}

void MP3File::parseID3v1(const std::string& ID3v1Block)
{
    title = StringUtils::trim(ID3v1Block.substr(titleRange.first, titleRange.second));
    artist = StringUtils::trim(ID3v1Block.substr(artistRange.first, artistRange.second));
    album = StringUtils::trim(ID3v1Block.substr(albumRange.first, albumRange.second));
    year = StringUtils::trim(ID3v1Block.substr(yearRange.first, yearRange.second));
    comment = StringUtils::trim(ID3v1Block.substr(commentRange.first, commentRange.second));
    genre = StringUtils::trim(ID3v1Block.substr(genreRange.first, genreRange.second));
}

MP3File::MP3File(std::ifstream& fileStream)
{
    fileStream.unsetf(std::ios::skipws);

    fileStream.seekg(0, std::ios_base::end);
    auto fileSize = fileStream.tellg();

    fileStream.seekg(0, std::ios_base::beg);

    fileData = std::vector<char>(fileSize);
    fileStream.read(fileData.data(), fileSize);

    std::string ID3v1 = getID3v1Block();

    // printf("ID3v1 block is: %s\n", ID3v1.c_str());
    // printf("ID3v1 block size is: %d\n", (int)ID3v1.length());

    //Commented out because of my wrong understandin of TAG+ block (it is placed before TAG block, not instead of it)
    // if (StringUtils::startsWith(ID3v1, "TAG+")) {
    //     puts("TAG+ is not supported!");
    //     return;
    // }

    if (StringUtils::startsWith(ID3v1, "TAG")) {
        parseID3v1(ID3v1);
    } else {
        std::string ID3v1EmptyBlock = "TAG" + std::string(125, 0);
        std::copy(ID3v1EmptyBlock.begin(), ID3v1EmptyBlock.end(), std::back_inserter(fileData));
    }
}

void MP3File::copyTAG(const std::string& content, std::pair<int, int>& where)
{
    const char* start = content.c_str();
    const char* end;
    if ((int)content.length() > where.second) {
        end = content.c_str() + where.second;
    } else {
        end = content.c_str() + content.length();
    }
    std::copy(start, end, fileData.end() - 128 + where.first);
}

void MP3File::updateID3v1Block()
{
    copyTAG(title, titleRange);
    copyTAG(artist, artistRange);
    // copyTAG(album, albumRange.first);
    // copyTAG(year, yearRange.first);
    // copyTAG(comment, commentRange.first);
    // copyTAG(genre, genreRange.first);
}
