#include "StringUtils.h"

#include <algorithm>

namespace StringUtils {
std::string trim(const std::string& source)
{
    std::string stripped;

    auto stripS = source.find_first_not_of('\0');
    auto stripE = source.find_last_not_of('\0');
    if (stripS != stripE)
        stripped = source.substr(stripS, (stripE - stripS) + 1);
    else
        stripped = source;

    auto start = stripped.find_first_not_of(' ');
    auto end = stripped.find_last_not_of(' ');
    std::string trimmed = stripped.substr(start, (end - start) + 1);

    return trimmed;
}

bool startsWith(const std::string& source, const std::string& find)
{
    bool sizeCheck = find.size() <= source.size();

    return sizeCheck && source.compare(0, find.length(), find) == 0;
}
bool endsWith(const std::string& source, const std::string& find)
{
    bool sizeCheck = find.size() <= source.size();
    int start = source.length() - find.length();

    return sizeCheck && source.compare(start, find.length(), find) == 0;
}
}