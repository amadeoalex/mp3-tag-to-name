// #define UNICODE

#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <windows.h>

#include "MP3File.h"
#include "StringUtils.h"

std::vector<std::string> listMP3FilesInDirectory(const std::string& direcotry)
{
    //too bad that current mingw64 8.1.0 has problems with <filesystem>
    std::vector<std::string> files;

    DIR* dir = opendir(direcotry.c_str());
    struct dirent* dirent;
    while ((dirent = readdir(dir)) != NULL) {
        opendir(dirent->d_name);
        if (errno == ENOTDIR) {
            std::string fileName(dirent->d_name);
            if (StringUtils::endsWith(fileName, ".mp3"))
                files.push_back(fileName);
        }
    }
    closedir(dir);

    return files;
}

std::optional<std::ifstream> openFileStream(const std::string& filePath)
{
    std::ifstream fileStream;
    fileStream.open(filePath, std::ios::binary);

    if (!fileStream.good()) {
        puts("File access error!");
        return {};
    }

    if (!fileStream.is_open()) {
        puts("Cannot open file!");
        return {};
    }

    return fileStream;
}

void saveFile(const std::string& path, const std::string& fileName, std::vector<char>& data)
{
    //c++17 <filesystem> again -.-
    if (CreateDirectoryA(path.c_str(), NULL) || GetLastError() == ERROR_ALREADY_EXISTS) {
        std::string filePath = path + "\\" + fileName;

        std::ofstream outputStream;
        outputStream.open(filePath, std::ios::out | std::ios::binary);

        outputStream.write(data.data(), data.size());
        outputStream.close();
    } else {
        puts("Error creating output direcotry!");

        return;
    }
}

std::map<std::string, MP3File> parseFilesInDirectory(const std::string& workingDirectory, const std::string& outputDirecotory)
{
    std::map<std::string, MP3File> invalidFiles;

    std::vector<std::string> files = listMP3FilesInDirectory(workingDirectory);
    if (files.size() == 0) {
        puts("No .mp3 files in current direcotry!");
        return invalidFiles;
    }

    for (std::string file : files) {
        auto fileStream = openFileStream(file);
        if (fileStream.has_value()) {
            printf("Parsing: %s\n", file.c_str());
            MP3File mp3(fileStream.value());

            if (mp3.artist.length() > 0 && mp3.title.length() > 0) {
                std::string fileName = mp3.artist + " - " + mp3.title + ".mp3";

                saveFile(outputDirecotory, fileName, mp3.fileData);
            } else {
                printf("File '%s', does not have required tags!\n", file.c_str());
                invalidFiles[file] = mp3;
            }

            fileStream.value().close();
        }
    }

    return invalidFiles;
}

void printInstructions()
{
    std::cout << "\nCopies mp3 file to 'renamed' directory whilst changing file name to [ARTIST - TITLE.mp3] format based on information contained within tag data." << '\n'
              << "If tag data does not contain required information, file is copied without changing its name."
              << "\n\n"
              << "Usage: MP3TAGTONAME [WORKING_DIR] [OUTPUT_DIR]" << '\n'
              << "  [OUTPUT_DIR] - optional, './renamed/' it not specified" << '\n'
              << "Example: " << '\n'
              << "  MP3TAGTONAME ./music/" << '\n';
}

void addMissingTags(std::map<std::string, MP3File>& invalidFiles, const std::string& outputDirecotory)
{
    for (auto& [file, mp3] : invalidFiles) {
        std::cout << "\nWorking with file: " << file << '\n';

        std::cout << "Artist?: ";
        std::cin >> std::ws;
        std::string artist;
        std::getline(std::cin, artist, '\n');

        std::cout << "Title?: ";
        std::cin >> std::ws;
        std::string title;
        std::getline(std::cin, title, '\n');

        std::cout << "Artist: " << artist << '\n';
        std::cout << "Title: " << title << '\n';

        mp3.artist = artist;
        mp3.title = title;
        mp3.updateID3v1Block();

        std::string fileName = file;
        saveFile(outputDirecotory, fileName, mp3.fileData);
    }
}

int main(int argc, char* argv[])
{
    if (argc < 2) {
        printInstructions();
        return 0;
    }

    std::string workingDirecotry = argv[1];
    std::string outputDirecotory;
    if (argc >= 3)
        outputDirecotory = argv[2];
    else
        outputDirecotory = ".\\renamed";

    auto invalidFiles = parseFilesInDirectory(workingDirecotry, outputDirecotory);
    if (invalidFiles.size() > 0) {
        puts("File(s) without required tags was/were found, would you like to add them manually? [Y/n] ");
        std::string response;
        std::cin >> response;
        if (response == "y" || response == "Y")
            addMissingTags(invalidFiles, outputDirecotory);
    }

    return 0;
}